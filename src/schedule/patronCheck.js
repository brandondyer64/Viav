const { client } = require('../discordClient')
const schedule = require('node-schedule')
const configSecret = require('../config.secret')
const strings = require('../strings')
const { mergeConfig } = require('../util/guildFunctions')

function check() {
  // Get master guild
  const masterGuild = client.guilds.find(
    val => val.id == configSecret.masterGuild
  )
  // Step through guilds
  const guilds = client.guilds.array()
  for (i in guilds) {
    // Get owner roles
    const guild = guilds[i]
    const owner = guild.owner
    const ownerMember = masterGuild.members.find(val => val.id == owner.id)
    if (!ownerMember) {
      mergeConfig(guild.id, { maxBots: 2 })
      continue
    }
    const ownerRoles = ownerMember.roles
    // Extract roles
    let maxBots = 2
    for (j in configSecret.patronRoles) {
      const patronRole = configSecret.patronRoles[j]
      if (ownerRoles.has(patronRole.id) && patronRole.bots > maxBots) {
        maxBots = patronRole.bots
      }
    }
    // Set guild config new max
    mergeConfig(guild.id, { maxBots })
  }
}

// Every night at midnight
schedule.scheduleJob('0 0 * * *', check)

// On startup
check()
