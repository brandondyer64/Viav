const express = require('express')
const app = express()
const { getConfig } = require('../util/guildFunctions')

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  )
  next()
})

app.get('/', (req, res) => res.send('Viav API'))

app.get('/getconfig/:id', (req, res) => {
  const guildId = req.param('id')
  const config = getConfig(guildId)
  const configDisplay = {}
  for (key in config) {
    configDisplay[key] = key.camelToWords()
  }
  res.json({ config, configDisplay })
})

app.get('/signin/:token', require('./signin'))

app.listen(3000, () => console.log('Viav API listening on port 3000'))
