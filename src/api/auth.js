const crypto = require('crypto')
const configSecret = require('../config.secret.json')

module.exports = {
  validateToken(token) {
    // Extract values
    const tokenSplit = token.split('G')
    if (!tokenSplit.length === 3)
      return { valid: false, error: 'no_token', errorText: 'Not a token' }
    let [guildId, expire, sig] = tokenSplit
    // Check expire time
    var d = new Date()
    var seconds = Math.round(d.getTime() / 1000) - 1535906070
    if (seconds > expire) {
      return {
        valid: false,
        error: 'expired',
        errorText: 'Token is expired. Please generate a new one'
      }
    }
    // Generate signature
    const uToken /* unsigned token */ = `${guildId}G${expire}`
    const hash = crypto.createHash('sha256')
    hash.update(uToken + '$ $' + configSecret.token)
    const cSig /* calculated sig */ = hash
      .digest('hex')
      .substring(0, 8)
      .toUpperCase()
    // Check signatures
    if (sig !== cSig) {
      return {
        valid: false,
        error: 'signature_mismatch',
        errorText: 'Signatures mismatch. Try regenerating the token.'
      }
    }
    return { valid: true, guildId, seconds, expire, timeLeft: expire - seconds }
  }
}
