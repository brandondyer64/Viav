const auth = require('./auth.js')

module.exports = (req, res) => {
  const token = req.param('token')
  const authVals = auth.validateToken(token)
  res.json(authVals)
}
