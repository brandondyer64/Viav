const { client } = require('./discordClient')
const config = require('./config.json')
const configSecret = require('./config.secret.json')
const { commands, applyCommands } = require('./commander')

// Log errors
client.on('error', console.error)

// Method utilities
require('./util/methods')

// Events
require('./events/botJoin')
require('./events/channel')
require('./events/userJoin')
require('./events/reactionVote')

// Message Validations
require('./validations/invite')

// Commands
require('./commands/music')
require('./commands/plugins')
require('./commands/set')
require('./commands/help')
require('./commands/invite')
require('./commands/changelog')
require('./commands/misc')
require('./commands/botAdmin')
require('./commands/authCode')
require('./commands/patron')

// Web API
require('./api/index.js')

// Initalize commands
applyCommands(client, commands)

/**
 * Log in the bot.
 *
 * @type {Object}
 */
client.login(configSecret.token).then(() => {
  client.user.setPresence({
    game: {
      name: `viav.app`,
      type: 'WATCHING'
    },
    status: 'online'
  })
  config.musicBots = [client.user.id]
  // Schedules
  require('./schedule/patronCheck')
})
