const { client } = require('./discordClient')
const config = require('./config.json')
const configSecret = require('./config.secret.json')
const { sendMessage } = require('./messager')
const { getConfig } = require('./util/guildFunctions')
const { getLang } = require('./util/lang')

const commands = {}
const plugins = {}
exports.commands = commands
exports.plugins = plugins

const validations = []
exports.validations = validations

function applyPlugins() {
  console.log('Loading plugins:')
  // Get all plugins
  for (let pluginI in plugins) {
    let plugin = plugins[pluginI]
    process.stdout.write(pluginI + ' ')
    // Get plugin's commands
    for (commandI in plugin.commands) {
      process.stdout.write('.')
      let command = plugin.commands[commandI]
      // Add plugin data to command
      command.plugin = pluginI
      commands[commandI] = command
    }
    console.log('')
  }
  console.log('Done.')
}

/**
 * Apply commands to the bot and handle them accordingly.
 *
 * @param  {Object} client   The Discord client object.
 * @param  {Object} commands The commands.
 */
exports.applyCommands = (client, commands) => {
  applyPlugins()
  client.on('message', message => {
    // Get guild config
    if (!message.guild) return
    const guildConfig = getConfig(message.guild.id)
    const lang = guildConfig.lang
    // Validate Message
    let valid = true
    for (const i in validations) {
      const validation = validations[i]
      valid &= !(
        validation.run({ guild: message.guild, message, lang, guildConfig }) ===
        false
      )
    }
    if (!valid) return
    // Check if the message is for me
    if (
      message.mentions &&
      message.mentions.members.array()[0] === message.guild.me
    ) {
      sendMessage(message.channel, `Hi! \`${guildConfig.prefix}help\` for help`)
      return
    }
    // Check that message is a command
    if (!message.content.startsWith(guildConfig.prefix)) return
    const memberName = message.member
      ? message.member.nickname || message.member.displayName
      : message.author.username
    console.log(
      (message.guild.name + ':').padding(20) +
        (memberName + ':').padding(20) +
        message.content
    )
    // Get command params
    const params = message.content.substr(guildConfig.prefix.length).split(' ')
    const command = params[0].toLowerCase()
    params.shift()
    // Delete command
    /*
    setTimeout(() => {
      if (message.deletable) message.delete()
    }, 1000)*/

    if (guildConfig.deleteMessages === true) {
      message.delete()
    }
    // Check that command exists
    if (!(command in commands)) {
      sendMessage(
        message.channel,
        `The command **${command}** doesn't exist! Try **${
          config.prefix
        }help**`,
        5,
        11023412
      )
      return
    }
    // Check if command requires admin
    if (commands[command].admin) {
      if (configSecret.admins.includes(message.author.id)) {
        // User has admin
        sendMessage(message.channel, 'Admin authorized.', 2, 2787114)
      } else {
        // User does not have admin
        sendMessage(
          message.channel,
          `You must be an admin to run this command.`,
          5,
          11023412
        )
        return
      }
    }
    // Check if command requires owner
    if (commands[command].owner) {
      if (message.member.id === message.member.guild.ownerID) {
        // User is owner
        sendMessage(message.channel, 'Owner authorized.', 2, 2787114)
      } else if (configSecret.admins.includes(message.author.id)) {
        // User has admin
        sendMessage(message.channel, 'Admin authorized.', 2, 2787114)
      } else {
        // User does not have admin
        sendMessage(
          message.channel,
          `You must be the server owner to run this command.`,
          5,
          11023412
        )
        return
      }
    }
    // Run command
    try {
      let callback = responseText => {
        // Echo command response
        if (responseText) {
          sendMessage(
            message.channel,
            responseText,
            commands[command].timeout || null
          )
        }
      }
      if (commands[command].plugin) {
        commands[command].run({
          params,
          message,
          callback,
          lang: getLang(message),
          guildConfig,
          guild: message.guild
        })
      } else {
        commands[command].run(params, message, callback, getLang(message))
      }
    } catch (err) {
      sendMessage(message.channel, `**What?**\n${err.message}`, 10, 11023412)
      console.error(err)
    }
  })
}
