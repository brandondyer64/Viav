const { client } = require('../discordClient')
const request = require('request').defaults({ encoding: null })
const { createPlugin } = require('../plugin')
const { commands } = require('../commander')
const configSecret = require('../config.secret.json')
const fetch = require('node-fetch')

createPlugin('BotAdmin')
  .command('setusername', {
    admin: true,
    run: ({ params, message, callback, lang }) => {
      client.user.setUsername(params.join(' '))
    }
  })
  .command('seticon', {
    admin: true,
    run: ({ params, message, callback }) => {
      request(params[0], function(err, res, body) {
        if (!err && res.statusCode === 200) {
          var data =
            'data:' +
            res.headers['content-type'] +
            ';base64,' +
            new Buffer(body).toString('base64')
          client.user.setAvatar(data, callback())
        }
      })
    }
  })
  .command('servercount', {
    admin: true,
    run: ({ params, message, callback }) => {
      callback(`${client.guilds.array().length}`)
    }
  })
  .command('usercount', {
    admin: true,
    run: ({ params, message, callback }) => {
      var count = 0
      const guilds = client.guilds.array()
      for (var i in guilds) {
        const guild = guilds[i]
        count += guild.memberCount
      }
      callback(`${count} users.`)
    }
  })
  .command('serverlist', {
    admin: true,
    run: ({ params, message, callback }) => {
      var output = ''
      const guilds = client.guilds.array()
      for (var i in guilds) {
        const guild = guilds[i]
        output += guild.name + ' - ' + guild.memberCount + '\n'
      }
      callback(output)
    }
  })
  .command('serverinvites', {
    admin: true,
    run: ({ params, message, callback }) => {
      const guilds = client.guilds.array()
      var inviteNum = 0
      var inviteStr = ''
      const promises = []
      for (var i in guilds) {
        const guild = guilds[i]
        promises.push(
          new Promise((resolve, reject) => {
            if (params[0] && params[0] != guild.owner.id) {
              resolve()
              return
            }
            try {
              guild
                .fetchInvites()
                .then(invites => {
                  const invite = invites.array()[0]
                  console.log(`Fetched ${invites.size} invites`)
                  if (invite)
                    inviteStr += 'https://discord.gg/' + invite.code + '\n'
                  resolve()
                })
                .catch(err => {
                  console.error(err)
                  resolve()
                })
            } catch (e) {}
          })
        )
      }
      Promise.all(promises)
        .then(values => {
          message.channel.send(inviteStr)
        })
        .catch(console.error)
    }
  })
  .command('restartchildren', {
    admin: true,
    run({ params, message, callback }) {
      for (i in configSecret.children) {
        const child = configSecret.children[i]
        if (child.enabled) {
          fetch(`${child.ip}/shutdown`, {
            method: 'POST',
            body: JSON.stringify({}),
            headers: { 'Content-Type': 'application/json' }
          })
            .then(response => {
              return response.json()
            }, console.error)
            .then(data => {
              console.log(data)
            })
            .catch(console.error)
        }
      }
    }
  })
