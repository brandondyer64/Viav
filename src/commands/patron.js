const core = require('./core').corePlugin
const { client } = require('../discordClient')
const schedule = require('node-schedule')
const configSecret = require('../config.secret')
const strings = require('../strings')
const { mergeConfig } = require('../util/guildFunctions')

core.command('patron', {
  owner: true,
  run({ params, message, callback, lang, guild, guildConfig }) {
    // Get master guild
    const masterGuild = client.guilds.find(
      val => val.id == configSecret.masterGuild
    )
    // Get owner roles
    const owner = guild.owner
    const ownerMember = masterGuild.members.find(val => val.id == owner.id)
    if (!ownerMember) {
      mergeConfig(guild.id, { maxBots: 2 })
      callback(
        'Please join the [discord server](https://discord.gg/dd4pE7e) ' +
          'and select a [plan](https://www.patreon.com/join/viav)'
      )
      return
    }
    const ownerRoles = ownerMember.roles
    // Extract roles
    let maxBots = 2
    for (j in configSecret.patronRoles) {
      const patronRole = configSecret.patronRoles[j]
      if (ownerRoles.has(patronRole.id) && patronRole.bots > maxBots) {
        maxBots = patronRole.bots
      }
    }
    // Set guild config new max
    mergeConfig(guild.id, { maxBots })
    // Give user feedback
    callback(`Your server can now use up to ${maxBots} bots.`)
  }
})
