const { client } = require('../discordClient')
const request = require('request').defaults({ encoding: null })
const { createPlugin } = require('../plugin')
const { commands } = require('../commander')
const { corePlugin } = require('./core')
const configSecret = require('../config.secret.json')
const crypto = require('crypto')
const { sendDM } = require('../messager')

corePlugin.command('auth', {
  owner: true,
  run: ({ params, message, callback, lang, guild }) => {
    // Create expire time
    var d = new Date()
    var seconds = Math.round(d.getTime() / 1000) - 1535906070
    const expireSeconds = seconds + 3 * 24 * 60 * 60
    // Create payload
    const payload = `${guild.id}G${expireSeconds}`
    // Sign payload
    const hash = crypto.createHash('sha256')
    hash.update(payload + '$ $' + configSecret.token)
    const sig = hash
      .digest('hex')
      .substring(0, 8)
      .toUpperCase()
    const signedPayload = `${payload}G${sig}`
    // Return payload to user
    sendDM(
      message.author,
      "Here's your access token:\n```" +
        signedPayload +
        '```\nIt will last for a few days.\n' +
        "Don't give it to anyone!"
    )
  }
})
