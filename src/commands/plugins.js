const { corePlugin } = require('./core')

corePlugin.command('plugins', {
  owner: true,
  run: ({ guildConfig, callback }) => {
    let output = ''
    for (var i in guildConfig.plugins) {
      let isEnabled = guildConfig.plugins[i]
      output += `**${i}**: ` + (isEnabled ? 'Enabled :white_check_mark:' : 'Disabled :negative_squared_cross_mark:') + '\n'
    }
    callback(output)
  }
})