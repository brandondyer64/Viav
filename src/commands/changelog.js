const { client } = require('../discordClient')
const fs = require('fs')
const { corePlugin } = require('./core')
const { getConfig, getDefaultChannel } = require('../util/guildFunctions')

function getChangeLog() {
  return new Promise((resolve, reject) => {
    fs.readFile(`changelog.md`, 'utf8', (err, data) => {
      if (err) {
        reject('Changelog not found.')
        return
      }
      resolve(data)
    })
  })
}

corePlugin
  .command('changelog', {
    description: 'The change log',
    owner: true,
    run: ({params, message, callback}) => {
      fs.readFile(`changelog.md`, 'utf8', (err, data) => {
        if (err) {
          callback('Changelog not found.')
          return
        }
        callback(data)
      })
    }
  })
  .command('globalchangelog', {
    admin: true,
    run: ({params, message, callback}) => {
      for (let i in client.guilds.array()) {
        console.log('this is changelog')
        const guild = client.guilds.array()[i]
        if (getConfig(guild.id)['mutechangelog']) {
          continue
        }
        const channel = getDefaultChannel(guild)
        console.log(channel.id)
        getChangeLog().then(changelog => {
          channel.send({
            embed: {
              title: 'Change Log',
              description: changelog,
              color: 16766325
            }
          })
        })
      }
    }
  })
