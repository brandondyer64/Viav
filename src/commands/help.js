const { client } = require('../discordClient')
const config = require('../config.json')
const configSecret = require('../config.secret.json')
const { commands } = require('../commander')
const { getConfig } = require('../util/guildFunctions')
const strings = require('../strings.json')
const { corePlugin } = require('./core')

corePlugin.command('help', {
  alias: ['h', 'info'],
  description: 'Gives a command list or description of a command',
  run: ({ params, message, callback, lang }) => {
    const guildConfig = getConfig(message.guild.id)
    if (params.join(' ') === '') {
      let output =
        // Hi! I'm Viav
        `**${lang.hi}**\n${lang.Im} ` +
        '**[Viav](https://viav.app)**! _Pronounced "Vive"_.' +
        // Command List
        `\n\n**${lang.command_list}:**\n`
      // List commands
      for (let i in commands) {
        const command = commands[i]
        // Check conditions to print command
        if (
          // Plugin is disabled
          (!command.plugin || guildConfig.plugins[command.plugin]) &&
          // Command is hidden
          !command.hidden &&
          (!(command.owner || command.admin) || // Command doesn't require owner or admin
            // Author is admin
            configSecret.admins.includes(message.author.id) ||
            // Author is owner
            (!command.admin &&
              message.member.guild.owner.id === message.member.id))
        ) {
          let plugin = ''
          if (command.plugin) {
            plugin = ' _- ' + command.plugin + '_'
          }
          // Print command
          output += `\`${guildConfig.prefix}\`**${i}**${plugin}\n`
        }
      }
      output +=
        // Donation link
        `\n**${lang.like_this_bot}**\n` +
        `${lang.Consider} **[${lang.donating}](${strings.donate})**!\n` +
        // Help server
        `\n**${lang.need_more_help}**\n` +
        `${lang.Humans}: **${strings.helpserver}**`
      callback(output)
    } else {
      // Get command description
      let description
      if (lang[`command_${params[0]}`]) {
        description = lang[`command_${params[0]}`]
      } else {
        description = commands[params[0]].description
      }
      // Echo description
      callback(`**${params[0]}:**\n${description}`)
    }
  }
})
