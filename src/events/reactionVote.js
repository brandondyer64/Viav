const { client } = require('../discordClient')
const { getConfig } = require('../util/guildFunctions')
const { createPlugin } = require('../plugin')

// This is a great example plugin for events.

createPlugin('ReactionVote').clientOn('message', {
  guild: ([message]) => {
    return message.guild
  },
  run: ([message]) => {
    const guildConfig = getConfig(message.guild.id)
    if (message.content.includes(guildConfig.reactionVoteString)) {
      message.react(guildConfig.reactionVoteFirst).then(() => {
        message.react(guildConfig.reactionVoteSecond)
      })
    }
  }
})
