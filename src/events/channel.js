const { client } = require('../discordClient')
const { getConfig } = require('../util/guildFunctions')
const { createPlugin } = require('../plugin')

const exemptChannels = ['AFK', '--']

createPlugin('AutoVoice').clientOn('voiceStateUpdate', {
  guild: ([oldMember, newMember]) => {
    return (oldMember || newMember).guild
  },
  run: ([oldMember, newMember]) => {
    if (newMember.voiceChannel !== oldMember.voiceChannel) {
      let justLeft = false
      // Delete empty channel
      if (oldMember.voiceChannel != null) {
        // Check that the voice channel isn't exempt and has no members
        if (
          !exemptChannels.includes(oldMember.voiceChannel.name) &&
          oldMember.voiceChannel.members.size === 0
        ) {
          // Delete channel
          oldMember.voiceChannel.delete().catch(() => {})
        }
        // If I'm the only one left. I leave
        if (
          !exemptChannels.includes(oldMember.voiceChannel.name) &&
          oldMember.voiceChannel.members.size === 1 &&
          oldMember.voiceChannel === oldMember.guild.me.voiceChannel
        ) {
          // Try to leave channel
          try {
            justLeft = true
            // Dissolve dispatcher
            if (
              oldMember.voiceChannel.connection &&
              oldMember.voiceChannel.connection.dispatcher
            ) {
              oldMember.voiceChannel.connection.dispatcher.end()
            }
            // Leave
            oldMember.voiceChannel.leave()
          } catch (err) {}
        }
      }
      // Create new channel
      if (newMember.voiceChannel != null) {
        console.log(
          (newMember.guild.name + ':').padding(20) +
            ((newMember.nickname || newMember.displayName) + ':').padding(20) +
            'joined ' +
            newMember.voiceChannel.name.padding(20)
        )
        // Check that the voice channel isn't exempt and has no members
        if (
          !exemptChannels.includes(newMember.voiceChannel.name) &&
          newMember.voiceChannel.members.size === 1
        ) {
          // Create new channel
          const channelName = newMember.voiceChannel.name
          const channelParent = newMember.voiceChannel.parent
          const channelPosition = newMember.voiceChannel.position
          const voiceChannel = newMember.voiceChannel
          const userLimit = newMember.voiceChannel.userLimit
          setTimeout(() => {
            voiceChannel
              .clone()
              .then(channel => {
                // Set channel category
                if (channelParent) {
                  channel
                    .setParent(channelParent)
                    .then(() => {
                      newMember.guild.setChannelPosition(
                        channel,
                        channelPosition
                      )
                    })
                    .catch(() => {})
                }
                // Set channel admin
                const guildConfig = getConfig(newMember.guild.id)
                console.log(guildConfig.autoSetPermissions)
                if (guildConfig.autoSetPermissions === true) {
                  voiceChannel
                    .overwritePermissions(newMember, {
                      MANAGE_CHANNELS: true,
                      MOVE_MEMBERS: true
                    })
                    .catch(() => {})
                }
                // Set user limit
                channel.setUserLimit(userLimit)
              })
              .catch(() => {})
          }, 3000)
        }
        // Join channel
        /*if (
          justLeft &&
          (newMember.guild.me.voiceChannel == null ||
            newMember.guild.me.voiceChannel.members.size === 1)
        ) {
          newMember.voiceChannel.join()
        }*/
      }
    }
  }
})
