const fs = require('fs')
const config = require('../config.json')
const { mergeDeep } = require('./objectMerger')

const guildConfigs = new Map()
const cachedConfigs = new Map()

function getConfig(guildId) {
  if (!cachedConfigs.has(guildId)) {
    const guildConfig = getGuildConfig(guildId)
    if (guildConfig) {
      let cachedConfig = mergeDeep(config, guildConfig)
      cachedConfigs.set(guildId, cachedConfig)
      return cachedConfig
    } else {
      return config
    }
  }
  return cachedConfigs.get(guildId)
}

exports.getConfig = getConfig

function getGuildConfig(guildId) {
  if (!guildId) {
    console.log('Guild is not defined')
    return {}
  }
  if (guildConfigs.has(guildId)) {
    // Return current config
    return guildConfigs.get(guildId)
  } else {
    // Read guild config
    try {
      const data = fs.readFileSync(`src/guildConfigs/${guildId}.json`)
      // Return guild config
      if (data) {
        try {
          guildConfig = JSON.parse(data)
          guildConfigs.set(guildId, guildConfig)
          return guildConfig
        } catch (err) {
          console.error(err)
        }
      }
    } catch (e) {
      // Create guild config file
      fs.writeFile(`src/guildConfigs/${guildId}.json`, '{}', function(err) {
        if (err) {
          console.log(err)
          resolve({})
        }
        console.log('Empty guild config created!')
        guildConfigs.set(guildId, {})
      })
    }
    return null
  }
}

exports.getGuildConfig = getGuildConfig

exports.setConfig = (guildId, key, value) => {
  const output = {}
  output[key] = value
  exports.mergeConfig(guildId, output)
}

exports.mergeConfig = (guildId, value) => {
  let guildConfig = getGuildConfig(guildId)
  guildConfig = mergeDeep(guildConfig, value)
  guildConfigs.set(guildId, guildConfig)
  cachedConfigs.set(guildId, mergeDeep(config, guildConfig))
  fs.writeFile(
    `src/guildConfigs/${guildId}.json`,
    JSON.stringify(guildConfig, null, 2),
    function(err) {
      if (err) {
        return console.log(err)
      }

      console.log('Guild config update saved!')
    }
  )
}

exports.getDefaultChannel = guild => {
  // Check for a "general" channel, which is often default chat
  if (guild.channels.exists('name', 'general'))
    return guild.channels.find('name', 'general')

  // get "original" default channel
  if (guild.channels.has(guild.id)) return guild.channels.get(guild.id)

  // Get system channel
  if (guild.systemChannel) return guild.systemChannel

  // Now we get into the heavy stuff: first channel in order where the bot can speak
  // hold on to your hats!
  return guild.channels
    .filter(
      c =>
        c.type === 'text' &&
        c.permissionsFor(guild.client.user).has('SEND_MESSAGES')
    )
    .sort(
      (a, b) =>
        a.position - b.position ||
        Long.fromString(a.id)
          .sub(Long.fromString(b.id))
          .toNumber()
    )
    .first()
}
