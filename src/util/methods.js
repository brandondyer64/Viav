String.prototype.padding = function(n, c) {
  const val = this.valueOf()
  if (Math.abs(n) <= val.length) {
    return val
  }
  const m = Math.max(Math.abs(n) - this.length || 0, 0)
  const pad = Array(m + 1).join(String(c || ' ').charAt(0))
  //      var pad = String(c || ' ').charAt(0).repeat(Math.abs(n) - this.length);
  return n < 0 ? pad + val : val + pad
  //      return (n < 0) ? val + pad : pad + val;
}

String.prototype.camelToWords = function() {
  const val = this.valueOf()
  return (
    val
      // insert a space before all caps
      .replace(/([A-Z])/g, ' $1')
      // uppercase the first character
      .replace(/^./, function(str) {
        return str.toUpperCase()
      })
  )
}
