const { mergeDeep } = require('./objectMerger')
const { getConfig } = require('./guildFunctions')

// Get base language files
const de = require('../lang/de.json')
const en = require('../lang/en.json')
const eo = require('../lang/eo.json')
const es = require('../lang/es.json')
const fi = require('../lang/fi.json')
const fr = require('../lang/fr.json')
const tr = require('../lang/tr.json')

// Compile languages
const langson = {
  en,
  de: { ...en, ...de },
  eo: { ...en, ...eo },
  es: { ...en, ...es },
  fi: { ...en, ...fi },
  fr: { ...en, ...fr },
  tr: { ...en, ...tr }
}
exports.langson = langson

// Return a string based on locale and key
exports.langl = (locale, key) => {
  return langson[locale][key]
}

// Return a string based on a key and locale derived from guild config
exports.lang = (message, key) => {
  return exports.langl(getConfig(message.guild.id)['lang'], key)
}

// Return a language based on locale derived from guild config
exports.getLang = message => {
  return langson[getConfig(message.guild.id)['lang']]
}
